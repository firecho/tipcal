//
//  ViewController.swift
//  TipCal
//
//  Created by Ernie Cho on 3/20/17.
//  Copyright © 2017 ErnShu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var tipLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    var firstNumberText = "0"
    var secondNumberText = "0"
    var op = ""
    var isFirstNumber = true
    var hasOp = false
    var canClear = true
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func handleButtonTips(_ sender: UIButton) {
        let checkAmount = Double(resultLabel.text!)
        var tipAmount : Double = 0
        var totalAmount : Double = 0
        switch sender.tag {
        case 1:
            tipAmount = checkAmount! * 0.12
            totalAmount = tipAmount + checkAmount!
            tipLabel.text = String(format: "%.2f", tipAmount)
            totalLabel.text = String(format: "%.2f", totalAmount)
        break
        case 2:
            tipAmount = checkAmount! * 0.15
            totalAmount = tipAmount + checkAmount!
            tipLabel.text = String(format: "%.2f", tipAmount)
            totalLabel.text = String(format: "%.2f", totalAmount)
        break
        case 3:
            tipAmount = checkAmount! * 0.18
            totalAmount = tipAmount + checkAmount!
            tipLabel.text = String(format: "%.2f", tipAmount)
            totalLabel.text = String(format: "%.2f", totalAmount)
        break
        case 4:
            tipAmount = checkAmount! * 0.2
            totalAmount = tipAmount + checkAmount!
            tipLabel.text = String(format: "%.2f", tipAmount)
            totalLabel.text = String(format: "%.2f", totalAmount)
        break
        default:
            resultLabel.text = "ERROR"
            return
        }
        
    }
    
    
    @IBAction func clearButton(_ sender: UIButton) {
        resultLabel.text = "0"
        tipLabel.text = "0"
        totalLabel.text = "0"
        firstNumberText = "0"
        secondNumberText = "0"
        op = ""
        isFirstNumber = true
        hasOp = false
        canClear = true
    }
    
    
    
    @IBAction func handleButtonPress(_ sender: UIButton) {
        if canClear {
            resultLabel.text = ""
            canClear = false
        }
        let currentText = resultLabel.text!
        let textLabel = sender.titleLabel?.text
        if let text = textLabel {
            switch text {
                case "+", "*", "/", "-":
                    if hasOp {
                        return
                }
                op = text
                isFirstNumber = false
                hasOp = true
                resultLabel.text = "\(currentText) \(op)"
                break
                case "=":
                isFirstNumber = true
                hasOp = false
                canClear = true
                let result = calculate()
                resultLabel.text = "\(result)"
                break
            default:
                if isFirstNumber {
                    firstNumberText = "\(firstNumberText)\(text)"
                } else {
                    secondNumberText = "\(secondNumberText)\(text)"
                }
                resultLabel.text = "\(currentText)\(text)"
                break;
                
                }
        }
    }

    
    
    func calculate() -> Double {
        let firstNumber = Double(firstNumberText)!
        let secondNumber = Double(secondNumberText)!
        firstNumberText = "0"
        secondNumberText = "0"
        switch op {
            case "+":
                return firstNumber + secondNumber
            case "-":
                return firstNumber - secondNumber
            case "*":
                return firstNumber * secondNumber
            case "/":
                return firstNumber / secondNumber
        default:
            return firstNumber
        }
    }
}

